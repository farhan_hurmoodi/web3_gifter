export const shortenWalletAddress = (address) => {
  address ||= "";
  const len = address.length;
  if (len <= 10) {
    return address;
  } else {
    return `${address.slice(0, 8)}...${address.slice(len - 6, len - 1)}`;
  }
};

export const ethTimeToDate = (ts) => {
  return new Date(parseInt(ts._hex, 16) * 1000);
};

export const ethTimeToString = (ts) => {
  const options = {
    weekday: "long",
    year: "numeric",
    month: "numeric",
    day: "numeric",
  };
  return ethTimeToDate(ts).toLocaleDateString("en-US", options);
};
