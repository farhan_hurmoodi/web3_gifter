import React, { useEffect, useState, useRef } from "react";
import { ethers } from "ethers";
import { toast } from "react-toastify";
import debounce from "lodash/debounce";

import { contractAddress, contractABI } from "../utils/constants";

export const TransactionContext = React.createContext();

const { ethereum } = window;

const SUPPORTED_CHAINS = [
  {
    hex: "0x3",
    dec: 3,
    name: "Ropsten Test Network",
  },
];

const supportedNetworkNames = SUPPORTED_CHAINS.map((c) => c.name).join(" or ");

const getEthereumContact = () => {
  const provider = new ethers.providers.Web3Provider(ethereum);
  const signer = provider.getSigner();

  return new ethers.Contract(contractAddress, contractABI, signer);
};

export const TransactionProvider = ({ children }) => {
  const [currentAccount, setCurrentAccount] = useState(null);
  const [currentNetwork, setCurrentNetwork] = useState(null);
  const [formData, setFormData] = useState({
    addressTo: "",
    amount: "",
    message: "",
    keyword: "",
  });
  const [isLoading, setLoading] = useState(false);

  // User<>wallet interactions functionalities
  const debouncedNetworkChange = useRef(
    debounce((networkId) => {
      onNetworkChanged(networkId);
    }, 300)
  ).current;

  const debouncedAccountChange = useRef(
    debounce((accounts) => {
      onAccountChanged(accounts);
    }, 300)
  ).current;

  useEffect(() => {
    return () => debouncedNetworkChange.cancel();
  }, [debouncedNetworkChange]);

  useEffect(() => {
    return () => debouncedAccountChange.cancel();
  }, [debouncedAccountChange]);

  const onNetworkChanged = (networkId) => {
    networkId = parseInt(networkId);
    // TO-DO:
    // Connect to https://chainlist.org/ to display network info.
    // For now, we will just force the user to connect to "Ropsten Test Network"
    if (!SUPPORTED_CHAINS.map((c) => c.dec).includes(networkId)) {
      setCurrentAccount(null);
      setCurrentNetwork(null);
      toast.error(`Please connect your wallet to ${supportedNetworkNames}`);
      return;
    }

    const connectedNetwork = SUPPORTED_CHAINS.filter(
      (n) => (n.dec = networkId)
    )[0];
    setCurrentNetwork(connectedNetwork);
    checkIfWalletIsConnected();
    toast.success(`Connected to ${connectedNetwork.name}`);
  };

  const onAccountChanged = (accounts) => {
    if (!currentNetwork) {
      setCurrentAccount(null);
      setCurrentNetwork(null);
      toast.error(`Please connect your wallet to ${supportedNetworkNames}`);
      return;
    }
    if (currentAccount != accounts[0]) {
      setCurrentAccount(accounts[0]);
      toast.warning(`Account has been changed to ${accounts[0]}`);
    }
  };

  ethereum && ethereum.on("accountsChanged", debouncedAccountChange);
  ethereum && ethereum.on("networkChanged", debouncedNetworkChange);

  const checkIfWalletIsConnected = async () => {
    try {
      if (!ethereum) {
        toast.error("Please install a wallet");
        return;
      }

      if (!SUPPORTED_CHAINS.map((c) => c.hex).includes(ethereum.chainId)) {
        toast.error(
          `Please change your wallet network to ${supportedNetworkNames}`
        );
        return;
      }

      const connectedNetwork = SUPPORTED_CHAINS.filter(
        (n) => (n.hex = ethereum.chainId)
      )[0];
      setCurrentNetwork(connectedNetwork);

      const accounts = await ethereum.request({ method: "eth_accounts" });
      if (accounts.length) {
        setCurrentAccount(accounts[0]);
      }
    } catch (error) {
      toast.error("Sorrt! Something went wrong when checking your account!");
      toast.error(error.message);
    }
  };

  const connectWallet = async () => {
    try {
      if (!ethereum) {
        toast.error("Please install a wallet");
        return;
      }
      if (!currentNetwork) {
        toast.error(
          `Please ensure you are connected to ${supportedNetworkNames}`
        );
        return;
      }

      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });
      setCurrentAccount(accounts[0]);
      toast.success("Successfully connected your wallet");
    } catch (error) {
      toast.error("Sorrt! Something went wrong when connecting your account!");
      toast.error(error.message);
    }
  };

  const handleFormChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const sendTransaction = async (e = null) => {
    try {
      if (e && e.preventDefault) {
        e.preventDefault();
      }
      if (!ethereum) {
        toast.error("Please install a wallet");
        return;
      }
      if (!currentAccount) {
        toast.error("Website is not connected to a wallet!");
        return;
      }

      const { addressTo, amount, keyword, message } = formData;

      if (!addressTo || !amount || !keyword || !message) {
        toast.error("One of the form field is empty");
        return;
      }

      setLoading(true);
      const parsedAmount = ethers.utils.parseEther(amount);
      await ethereum.request({
        method: "eth_sendTransaction",
        params: [
          {
            from: currentAccount,
            to: addressTo,
            gas: "0x5208", // 21000 Gwei
            value: parsedAmount._hex,
          },
        ],
      });

      const transactionContract = getEthereumContact();
      const transactionHash = await transactionContract.addToBlockchain(
        addressTo,
        parsedAmount,
        message,
        keyword
      );

      console.log("Loading - ", transactionHash.hash);
      await transactionHash.wait();
      toast("Transaction has been sent successfully");
    } catch (error) {
      toast.error(error.message);
    } finally {
      setLoading(false);
    }
  };

  const getAllTransactions = async () => {
    if (!currentAccount) {
      return [];
    }
    const transactionContract = getEthereumContact();
    return await transactionContract.getAllTransactions();
  };

  const getAllAccountTransactions = async () => {
    const transactions = await getAllTransactions();

    return transactions.filter(
      (tx) =>
        tx.sender.toLowerCase() == currentAccount ||
        tx.receiver.toLowerCase() == currentAccount
    );
  };

  useEffect(() => {
    checkIfWalletIsConnected();
  }, []);

  return (
    <TransactionContext.Provider
      value={{
        connectWallet,
        currentNetwork,
        currentAccount,
        formData,
        setFormData,
        handleFormChange,
        sendTransaction,
        getAllTransactions,
        getAllAccountTransactions,
        isLoading,
      }}
    >
      {children}
    </TransactionContext.Provider>
  );
};
