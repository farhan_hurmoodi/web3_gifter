import React from "react";
import ReactDOM from "react-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./index.css";
import App from "./App";
import { TransactionProvider } from "./context/TransactionContext";

ReactDOM.render(
  <React.StrictMode>
    <TransactionProvider>
      <App />
      <ToastContainer position="bottom-right" />
    </TransactionProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
