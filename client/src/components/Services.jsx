import { BsShieldFillCheck } from "react-icons/bs";
import { BiSearchAlt } from "react-icons/bi";
import { RiHeart2Fill } from "react-icons/ri";

const CARDS = [
  {
    title: "Security Guaranteed",
    description:
      "Security is guaranteed. We always maintain privacy and quality of our products.",
    icon: {
      color: "bg-[#2952e3]",
      component: <BsShieldFillCheck fontSize={21} className="text-white" />,
    },
  },
  {
    title: "Best exchange rates",
    description:
      "Security is guaranteed. We always maintain privacy and quality of our products.",
    icon: {
      color: "bg-[#9845f8]",
      component: <BiSearchAlt fontSize={21} className="text-white" />,
    },
  },
  {
    title: "Fastest transactions",
    description:
      "Security is guaranteed. We always maintain privacy and quality of our products.",
    icon: {
      color: "bg-[#f84550]",
      component: <RiHeart2Fill fontSize={21} className="text-white" />,
    },
  },
];

const ServiceCard = ({ title, description, icon }) => {
  return (
    <div className="flex flex-row justify-start items-start white-glassmorphism p-3 m-2 cursor-pointer hover:shadow-xl">
      <div
        className={`w-10 h-10 rounded-full flex justify-center items-center ${icon.color}`}
      >
        {icon.component}
      </div>
      <div className="flex flex-1 flex-col justify-start items-start text-white ml-5">
        <h3 className="font-bold mb-2 text-lg">{title}</h3>
        <p className="text-sm md:w-9/12">{description}</p>
      </div>
    </div>
  );
};

const Services = () => {
  return (
    <div className="flex w-full justify-center gradient-bg-services">
      <div className="flex mf:flex-row flex-col items-start justify-between md:p-20 py-12 px-4">
        <div className="flex flex-1 justify-start flex-col mf:mr-10">
          <h1 className="text-white text-3xl sm:text-5xl text-gradient py-1">
            Services that we
            <br />
            continue to improve
          </h1>
        </div>

        <div className="flex flex-1 flex-col justify-start items-center">
          {CARDS.map((card) => (
            <ServiceCard key={card.title} {...card} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Services;
