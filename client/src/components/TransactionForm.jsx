import { useContext } from "react";
import { SiEthereum } from "react-icons/si";
import { BsInfoCircle } from "react-icons/bs";

import Loader from "./Loader";
import { TransactionContext } from "../context/TransactionContext";
import { shortenWalletAddress } from "../utils/eth";

const EthCard = () => {
  const { currentAccount, currentNetwork } = useContext(TransactionContext);

  return (
    <div className="p-3 flex flex-col justify-end items-start rounded-xl h-40 sm:w-72 w-full my-5 eth-card glass-morphism">
      <div className="flex flex-col justify-between w-full h-full">
        <div className="flex justify-between items-start">
          <div className="w-10 h-10 rounded-full border-2 border-white flex justify-center items-center">
            <SiEthereum fontSize={21} color="#fff" />
          </div>
          <BsInfoCircle fontSize={17} color="#fff" />
        </div>

        <div>
          <div>
            <p className="text-white font-light text-sm">
              {shortenWalletAddress(currentAccount)}
            </p>
          </div>
          <div>
            <p className="text-white font-semibold text-lg mt-1">
              {(currentNetwork && currentNetwork.name) || "Ethereum"}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

const Input = (props) => {
  return (
    <input
      step="0.0001"
      className="my-2 w-full p-2 rounded-sm outline-none bg-transparent text-white border-none text-sm white-glassmorphism"
      {...props}
    />
  );
};

const TransactionForm = () => {
  const { isLoading, formData, handleFormChange, sendTransaction } =
    useContext(TransactionContext);

  return (
    <div className="flex flex-col flex-1 items-center justify-start w-full mf:mt-0 mt-10">
      <EthCard />
      <div className="p-5 sm:w-96 w-full flex flex-col justify-start items-center blue-glassmorphism">
        <Input
          placeholder="Address To"
          name="addressTo"
          type="text"
          value={formData.addressTo}
          onChange={handleFormChange}
        />
        <Input
          placeholder="Amount (ETH)"
          name="amount"
          type="number"
          value={formData.amount}
          onChange={handleFormChange}
        />
        <Input
          placeholder="Keyword (Gif)"
          name="keyword"
          type="text"
          value={formData.keyword}
          onChange={handleFormChange}
        />
        <Input
          placeholder="Enter Message"
          name="message"
          type="text"
          value={formData.message}
          onChange={handleFormChange}
        />
        <div className="h-[1px] w-full bg-gray-400 my-2 " />

        {isLoading ? (
          <Loader />
        ) : (
          <button
            type="button"
            className="text-white w-full mt-2 border-[1px] p-2 border-[#3d4f7c] rounded-full cursor-pointer"
            onClick={sendTransaction}
          >
            Send Now
          </button>
        )}
      </div>
    </div>
  );
};

export default TransactionForm;
