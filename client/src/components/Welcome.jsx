import { useContext } from "react";

import TransactionForm from "./TransactionForm";
import ConnectWalletButton from "./ConnectWalletButton";
import { TransactionContext } from "../context/TransactionContext";

const FeaturesGrid = () => {
  const commonStyles =
    "min-h-[70px] sm:px-0 px-2 sm:min-w-[120px] flex justify-center items-center \
  border-[0.5px] border-gray-400 text-sm font-light text-white";

  return (
    <div className="grid sm:grid-cols-3 grid-cols-2 w-full mt-10">
      <div className={`rounded-tl-2xl ${commonStyles}`}>Reliability</div>
      <div className={commonStyles}>Security</div>
      <div className={`rounded-tr-2xl ${commonStyles}`}>Ethereum</div>
      <div className={`rounded-bl-2xl ${commonStyles}`}>Web 3.0</div>
      <div className={commonStyles}>Low fees</div>
      <div className={`rounded-br-2xl ${commonStyles}`}>Blockchain</div>
    </div>
  );
};

const Welcome = () => {
  const { currentAccount } = useContext(TransactionContext);

  return (
    <div className="flex w-full justify-center">
      <div className="flex mf:flex-row flex-col items-start justify-between md:p-20 py-12 px-4">
        <div className="flex flex-1 justify-start flex-col mf:mr-10">
          <h1 className="text-white text-3xl sm:text-5xl text-gradient py-1">
            Send Crypto
            <br />
            accross the world
          </h1>
          <p className="text-white text-left mt-5 font-light md:w-9/12 w-11/12 text-base">
            Explore the Crypto world. Buy and sell cryptocurrencies easily on
            Krypto!
          </p>
          {!currentAccount && <ConnectWalletButton extraClassProps="mt-4" />}
          <FeaturesGrid />
        </div>
        <TransactionForm />
      </div>
    </div>
  );
};

export default Welcome;
