import React, { useContext } from "react";

import { TransactionContext } from "../context/TransactionContext";

const defaultClassName =
  "text-white text-base font-semibold flex flex-row justify-center items-center\
my-5 bg-[#2952e3] hover:bg-[#2546bd] p-3 rounded-full cursor-pointer";

const ConnectWalletButton = ({
  label = "Connect Wallet",
  classProps = defaultClassName,
  extraClassProps = "",
}) => {
  const { connectWallet } = useContext(TransactionContext);

  return (
    <button
      className={[classProps, extraClassProps].join(" ")}
      type="button"
      onClick={connectWallet}
    >
      {label}
    </button>
  );
};

export default ConnectWalletButton;
