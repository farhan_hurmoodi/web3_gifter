import { useState, useEffect, useContext } from "react";

import { TransactionContext } from "../context/TransactionContext";
import { shortenWalletAddress, ethTimeToString } from "../utils/eth";
import { useGiphyKeyword } from "../libs/giphy";

const TransactionCard = ({ transaction }) => {
  const image = useGiphyKeyword(transaction.keyword);
  const { currentAccount } = useContext(TransactionContext);
  const isSender = currentAccount == transaction.sender.toLowerCase();

  return (
    <div className="w-50 h-50 m-2 p-2 rounded bg-[black] white-glassmorphism text-white">
      <img
        src={image}
        alt="gif"
        className="w-64 h-64 2x:w-96 2x:h-96 rounded-md shadow-lg object-cover"
      />
      <p className="font-bold">{transaction.message}</p>
      <p className="text-sm">
        You {isSender ? "sent to " : "received from "}{" "}
        {isSender
          ? shortenWalletAddress(transaction.receiver)
          : shortenWalletAddress(transaction.sender)}
      </p>
      <p className="text-[gray] text-sm">
        {ethTimeToString(transaction.timestamp)}
      </p>
    </div>
  );
};

const Transactions = () => {
  const { getAllAccountTransactions, currentAccount } =
    useContext(TransactionContext);
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    getAllAccountTransactions().then((txs) => {
      setTransactions(txs);
    });
  }, [currentAccount]);

  return (
    <div className="flex w-full justify-center gradient-bg-transactions">
      <div className="flex mf:flex-row flex-col items-start justify-between md:p-20 py-12 px-4">
        <div className="flex flex-1 justify-start flex-col mf:mr-10">
          <h1 className="text-white text-3xl sm:text-5xl text-gradient py-1">
            Your transactions
          </h1>
          <div className="flex flex-row">
            {transactions.map((tx) => (
              <TransactionCard key={tx.hash} transaction={tx} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Transactions;
