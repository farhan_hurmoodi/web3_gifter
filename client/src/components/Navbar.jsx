import { useState, useContext } from "react";
import { HiMenuAlt4 } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";

import ConnectWalletButton from "./ConnectWalletButton";
import { TransactionContext } from "../context/TransactionContext";

import logo from "../../images/logo.png";

const NavbarItem = ({ title, classProps }) => {
  return <li className={`mx-4 cursor-pointer ${classProps}`}>{title}</li>;
};

const ITEMS = ["Market", "Exchange", "Tutorials", "Wallet"];

const Navbar = () => {
  const { currentAccount } = useContext(TransactionContext);
  const [toggleMenue, setToggleMenue] = useState(false);

  const navigationItems = (isMobile = false) => {
    const classProps = isMobile ? "my-2 text-lg" : "";
    return (
      <>
        {ITEMS.map((title, i) => (
          <NavbarItem key={i} title={title} classProps={classProps} />
        ))}
        {!currentAccount && (
          <li>
            <ConnectWalletButton
              label="Login"
              classProps="bg-[#2952e3] py-2 px-7 mx-4 rounded-full cursor-pointer hover:bg-[#2546bd]"
            />
          </li>
        )}
      </>
    );
  };

  return (
    <nav className="w-full flex md:justify-center justify-between items-center p-4">
      <div className="md:flex[0.5] flex-initial jutify-center items-center">
        <img src={logo} alt="logo" className="w-32 cursor-pointer" />
      </div>

      <ul className="md:flex text-white hidden list-none flex-row justify-center items-center flex-initial">
        {navigationItems()}
      </ul>
      <div className="flex relative">
        {toggleMenue ? (
          <AiOutlineClose
            fontSize={28}
            className="md:hidden cursor-pointer text-white"
            onClick={() => setToggleMenue(!toggleMenue)}
          />
        ) : (
          <HiMenuAlt4
            fontSize={28}
            className="md:hidden cursor-pointer text-white"
            onClick={() => setToggleMenue(!toggleMenue)}
          />
        )}
        {toggleMenue && (
          <ui
            className="
              md:hidden z-10 fixed top-0 right-0 p-3 w-[70vw] h-screen shadow-2xl list-none
              flex flex-col justify-start items-end rounded-md blue-glassmorphism text-white animte-slide-in
          "
          >
            <li className="text-xl w-full my-2">
              <AiOutlineClose
                fontSize={28}
                onClick={() => setToggleMenue(!toggleMenue)}
              />
            </li>

            {navigationItems(true)}
          </ui>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
