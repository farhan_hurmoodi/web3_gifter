import { useEffect, useState } from "react";

const GIPHY_API_KEY = import.meta.env.VITE_GIPHY_API_KEY;

export const useGiphyKeyword = (keyword) => {
  const [image, setImage] = useState(null);

  const fetchGif = async () => {
    try {
      const response = await fetch(
        `https://api.giphy.com/v1/gifs/search?api_key=${GIPHY_API_KEY}&limit=1&q=${keyword}`
      );
      const { data } = await response.json();

      setImage(data[0]?.images?.downsized_medium?.url);
    } catch (error) {}
  };

  useEffect(() => {
    if (keyword) {
      fetchGif();
    }
  }, [keyword]);

  return image;
};
